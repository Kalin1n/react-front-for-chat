import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import {Provider, connect}   from 'react-redux';                // REDUX
import {createStore, combineReducers} from 'redux';           // REDUX
import thunk from 'redux-thunk';                            //  Redux Thunk
import { applyMiddleware } from 'redux'

import {Router, Route, Link} from 'react-router-dom';
import createHistory from "history/createBrowserHistory";

var adress =  /*'https://kalininchatn.herokuapp.com/message'*/'http://localhost:4000/message'


// Fetches and functions

//  Обновить сообщения
function updateMessages(){
    var mesOnPage = document.getElementById('chat').length;                   // REDO
    fetch(`${adress}message`,
    {
      method : "GET"
    },mesOnPage)
    .then(function(res){
        return res.json();
      })
      .then( function(res){
        /*for ( var i = mesOnPage ; i < res.length ; i++){
        //
        //   }
          // var newLi = document.createElement('li');
          //   var div = document.createElement('div');
          //   var name = document.createElement('span');
          //   var message = document.createElement('span');
          //   var time = document.createElement('span');
          //   name.innerText = `${ res[i].nickname}`   // span
          //   message.innerText= res[i].message                   // span
          //
          //
          //   var t = new Date (res[i].timestamp)    // Time      // span
          //   time.innerHTML += `${t.getHours()} : `
          //   time.innerHTML += `${t.getMinutes()}`
          //
          //   div.appendChild(name);
          //   div.appendChild(time);
          //
          //
          //   //newLi.appendChild(name)
          //   newLi.appendChild(div)
          //   newLi.appendChild(message)
          //   //newLi.appendChild(time)
          // document.getElementById('chat').appendChild(newLi)*/
        }
      )
    .catch(function(res){console.log(res)})
}



// ------------------  REDUX

let store = createStore((state, action) => {
  if (state ===  undefined){
      console.log( { ...state,sendStatus:"EMPTY", status : "EMPTY"})
  }
  if(action.type === 'sendStatus'){
    console.log( {...state, sendStatus : action.sendStatus })
  }
  if (action.type === 'sendStatus'){
       console.log(  {...state, status: action.status, payload: action.payload, error: action.error} )
   }
  return state;
}, applyMiddleware(thunk));

const actionPending     = () => ({ type: 'SET_STATUS', status: 'PENDING1', payload: null, error: null })
const actionResolved    = payload => ({ type: 'SET_STATUS', status: 'RESOLVED1', payload, error: null })
const actionRejected    = error => ({ type: 'SET_STATUS', status: 'REJECTED1', payload: null, error })


function actionFetch(){
    return async function (dispatch){
        dispatch(actionPending())
        try {
            dispatch(actionResolved(await fetch(adress)))
        }
        catch (e) {
            dispatch(actionRejected(e))
        }
    }
}

function sendStatus(nickname, message ){
  fetch(adress,
         {
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json'
             },
             method: "POST",
             body: JSON.stringify({ nickname, message })
         })
         .then(() => (
            store.dispatch({
            type: "sendStatus",
            sendStatus:"RESOLVED"
        })
    ))

  return{
    type: 'sendStatus',
    sendStatus : 'PENDING'
  }
}


store.subscribe(()=> console.log(store.getState())) // подписка на обновления store, Отладка

let delay = ms => new Promise ( r => setTimeout(()=> r(ms),ms))

// Worstka Components

class Input extends Component{

constructor(props){
  super(props)
  this.state = { value : ''};

  this.nickOnChange = this.nickOnChange.bind(this);
  this.messageOnChange = this.messageOnChange.bind(this);
}

  nickOnChange (event){
    this.setState ({nickname: event.target.value})
  }
  messageOnChange (event){
    this.setState({message: event.target.value})
  }

  render(){
    return (
      <div className="fillarea">
          <form>
            <input type="text" placeholder="Type your nickname" id="nick" onChange = { this.nickOnChange}/*ref={c => this.nickname = c }  */ />
            <textarea type="text" placeholder="Type your message text" id="message"    onChange = { this.messageOnChange}/* ref={a => this.msg = a}*/></textarea>
          </form>
          <button type="button" name="button" id="send" onClick={ () => this.props.onSend ( this.state.nickname, this.state.message)  }>SEND <i className="fas fa-comment-alt"></i></button>
      </div>

    )
  }
}


let Connect = connect (null, {
  onSend : sendStatus
})(Input)


let Header = props => <>
    <div id="name" className="col-sm-12 col-md-12 top col-lg-12" >
      <h1>Chat on</h1>
      <img src={"Node.js.png" }alt="Node.js" className="nodepict" />
    </div>
  </>

// Chat Class with  loading and rendering messages and delay of update
class Chat extends Component{
  constructor(props){
    super()
    this.state = {data : [] }
  }
  async componentDidMount(){
  while (true){
    await delay(1000)
    let data = await (await fetch(adress)).json()
    this.setState({data})
    console.log(data);                    //  to write
    }
  }
  render (){
    return(
    <div className="history">
      <ul id="chat" className="chatHistory">{
        this.state.data.length ?
        this.state.data.map(  msg =>
            <li>
              <div className="messagePart">
                <span className="nickname">{`${msg.nickname} : `}</span>
                <span className="message">{msg.message}</span>
                </div>
                <span className="time">{Date(msg.timestamp)}</span>
                </li>)
        : "loading..."}
        </ul>
    </div>
  )
  }
}

let ConnectedView = connect(s => s)(props => <div>{props.sendStatus}</div>)

class About extends Component{


  render(){
    return(
      <div> Its a chat </div>
    )
  }
}
// Worstka end

// CLass App for all to render
class App extends Component {
  render() {
    return (

      <Provider store= {store}>
        <div className="App">
        <ConnectedView/>
            <Header/>
            <Connect/>

            <Chat/>

          </div>
        </Provider>
      

    );
  }
}

export default App;

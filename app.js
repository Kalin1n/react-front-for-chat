var express = require('express');
var app = express();
var cors = require('cors')
const bodyParser = require('body-parser');
var htttp = require('http');


app.use(cors())
app.use(express.static('public'));
app.use(bodyParser.json())

var port = 4000 //process.env.PORT ||
var messages = [/*{
    nickname : "Vasya"
    message : "Some text from Vasya"
  },
  {
    nickname : "Ivan",
    message : "Hello everybody!!! "
  }*/
];  // массив для сообщений


app.get('/message', function (req, res) {
  res.send(messages);
});


app.post('/message',function(req, res){
  app.use(bodyParser.json())

  req.body.timestamp = new Date().getTime();
  messages.push(req.body)
  res.status(201).send(req.body)
});



app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});


/*POST BY FETCH
  fetch("http://localhost:3000/message",
  {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({a: 1, b: 2})
  })
  .then(function(res){ console.log(res) })
  .catch(function(res){ console.log(res) })
*/
